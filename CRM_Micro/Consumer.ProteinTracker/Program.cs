﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;
using ServiceStack.ProteinTracker.Service;
using ServiceStack.Auth;

namespace Consumer.ProteinTracker
{
    class Program
    {
        static void Main(string[] args)
        {
            int amount = -1;
            
            //z autoryzacja 
            var client = new JsonServiceClient("http://localhost:51512"){UserName = "Joe",Password = "password"};

            //dodanie admina
            /*client.Send(new AssignRoles
            {
                UserName = "Joe",
                Roles = new List<string>{"User"},
                Permissions = new ArrayOfString("GetStatus")
            });*/

            while (amount != 0)
            {
                Console.WriteLine("Enter protein amount. 0 to exit");
                amount = int.Parse(Console.ReadLine());

                //normalnie
                //var response =client.Send(new Entry {Amount = amount, EntryTime = DateTime.Now});
                
                //asynchronicznie
                var response = client.SendAsync(new Entry { Amount = amount, EntryTime = DateTime.Now });

              
                //JsvServiceClient
            }
            StatusResponse statusResponse = null;
            try
            {
                //statusResponse = client.Post(new StatusQuery { Date = DateTime.Now });
                statusResponse = client.Patch<StatusResponse>((object) new StatusQuery { Date = DateTime.Now });
            }
            catch (WebServiceException ex)
            {

                Console.WriteLine(ex.ErrorMessage);
            }

          
            Console.WriteLine("{0} / {1}", statusResponse.Total,statusResponse.Goal);
            Console.WriteLine(statusResponse.Message);
            Console.ReadLine();
        }
    }
}
