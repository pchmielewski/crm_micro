﻿namespace ServiceStack.ProteinTracker.Service
{
    public class TrackedData
    {
        public int Total { get; set; }
        public int Goal { get; set; }
    }
}