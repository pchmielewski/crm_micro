﻿using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace ServiceStack.ProteinTracker.Service
{
    public class EntryService : ServiceStack.Service
    {
        public IDbConnectionFactory DbConnectionFactory { get; set; }

        public object Post(Entry request)
        {
            var date = request.EntryTime.Date;
            var trackedData = (TrackedData) SessionBag[date.ToString()] ?? new TrackedData {Goal = 300};

            trackedData.Total += request.Amount;
            SessionBag[date.ToString()] = trackedData;

            //again caching
            //remove cache from memory
            var cacheKey = UrnId.Create<StatusQuery>(request.EntryTime.ToShortDateString());
            base.Request.RemoveFromCache(base.Cache,cacheKey);

            //data base example
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                db.CreateTable<Entry>();
                db.Insert(request);
            }

            return new EntryResponse {Id = 1};
        }
    }
}