﻿using System;

namespace ServiceStack.ProteinTracker.Service
{
    [Route("/status")]
    [Authenticate]
    /*[RequiredRole("User")]
    [RequiredPermission("GetStatus")]*/
    public class StatusQuery : IReturn<StatusResponse>
    {
        public DateTime Date { get; set; } 
    }
}