﻿using System;
using ServiceStack.DataAnnotations;

namespace ServiceStack.ProteinTracker.Service
{
    [Route("/entry","POST")]
    [Route("/entry/{EntryTime}/{Amount}")]
    [RecordIpFilter]
    public class Entry : IReturn<EntryResponse>
    {
        [AutoIncrement]
        public int Id { get; set; }
        public DateTime EntryTime { get; set; }
        public int Amount { get; set; }
    }
}