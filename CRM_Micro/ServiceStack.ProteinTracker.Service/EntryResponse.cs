﻿namespace ServiceStack.ProteinTracker.Service
{
    public class EntryResponse
    {
        public int Id { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}