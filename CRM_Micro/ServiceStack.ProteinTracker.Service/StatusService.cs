﻿using System;
using System.Linq;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack;
using ServiceStack.Logging;

namespace ServiceStack.ProteinTracker.Service
{
    public class StatusService: ServiceStack.Service
    {
        public DbConnectionFactory DbConnectionFactory { get; set; }

        public object Any(StatusQuery request)
        {
            //throw new NotImplementedException("Error");

            var date = request.Date.Date;
            var trackedData = (TrackedData) SessionBag[date.ToString()];
            if (trackedData==null)
            {
                return trackedData = new TrackedData {Goal =300, Total = 0};
            }

            //caching
            var cacheKey = UrnId.Create<StatusQuery>(request.Date.ToShortDateString());

            //example of getting
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var total = db.Select<Entry>(e => e.EntryTime == request.Date).Sum(e => e.Amount);

            }
            
            //logging
            var log = LogManager.GetLogger(GetType());
            log.Info("Made status of......");

            var message = this.GetSession().DisplayName;

            /*return Request.ToOptimizedResultUsingCache(base.Cache, cacheKey,new TimeSpan(0,0,0,3), () =>
            {
               return new StatusResponse {Goal = trackedData.Goal, Total = trackedData.Total, Message = message};
            });*/

            return new StatusResponse {Goal = trackedData.Goal, Total = trackedData.Total, Message = message};
        }
    }
}