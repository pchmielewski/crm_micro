﻿using ServiceStack.Caching;
using ServiceStack.Web;

namespace ServiceStack.ProteinTracker.Service
{
    //szczegolowy 
    /*public class RecordIpFilter: IHasRequestFilter
    {
        public void RequestFilter(IRequest req, IResponse res, object requestDto)
        {
            throw new System.NotImplementedException();
        }

        public IHasRequestFilter Copy()
        {
            throw new System.NotImplementedException();
        }

        public int Priority
        {
            get { throw new System.NotImplementedException(); }
        }
    }*/

    public class RecordIpFilter : RequestFilterAttribute
    {
        //automatycznie wstrzykiwane jest tu memory cache z global.asax
        public ICacheClient Cache { get; set; }

        public override void Execute(IRequest req, IResponse res, object requestDto)
        {
            Cache.Add("last IP",req.UserHostAddress);
        }
    }

}