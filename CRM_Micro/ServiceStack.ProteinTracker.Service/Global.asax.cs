﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Funq;
using ServiceStack.Auth;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Logging.EventLog;
using ServiceStack.Messaging;
using ServiceStack.MiniProfiler;
using ServiceStack.MiniProfiler.Data;
using ServiceStack.OrmLite;
using ServiceStack.Razor;
using ServiceStack.Redis;
using ServiceStack.Validation;
using ServiceStack.Redis.RedisExtensions;

namespace ServiceStack.ProteinTracker.Service
{
    public class Global : System.Web.HttpApplication
    {
        public class ProteinTrackerAppHost : AppHostBase
        {
            public ProteinTrackerAppHost():base("Protein tracker", typeof(EntryService).Assembly)
            {
                
            }

            public override void Configure(Funq.Container container)
            {
                Plugins.Add(new AuthFeature(
                    () => new AuthUserSession(),
                    new IAuthProvider[]{new BasicAuthProvider(), new TwitterAuthProvider(new AppSettings())  } ));

                Plugins.Add(new RegistrationFeature());

                Plugins.Add(new ValidationFeature());
                container.RegisterValidators(typeof(EntryService).Assembly);

                //to moze tylko admin robic
                Plugins.Add(new RequestLogsFeature());

                LogManager.LogFactory = new EventLogFactory("ProteinTracker.Logging","Application");

                container.Register<ICacheClient>(new MemoryCacheClient());
                
                //redis
                //container.Register<IRedisClientsManager>(c=> new Pooled)
                
                var userRespository = new InMemoryAuthRepository();
                container.Register<IUserAuthRepository>(userRespository);

                var dbConnectionFactory =
                    new OrmLiteConnectionFactory(HttpContext.Current.Server.MapPath("~/App_Data/data.txt"), SqliteDialect.Provider)
                    {
                        ConnectionFilter = x=> new ProfiledDbConnection(x, Profiler.Current)
                    };
                container.Register<IDbConnectionFactory>(dbConnectionFactory);

                //sample user
                string hash;
                string salt;

                new SaltedHash().GetHashAndSaltString("password",out hash, out salt);
                userRespository.CreateUserAuth(new UserAuth
                {
                    Id = 1,
                    DisplayName = "Joe",
                    Email = "joe@o2.pl",
                    UserName = "Joe",
                    FirstName = "Joe",
                    LastName = "User",
                    PasswordHash = hash,
                    Salt = salt,
                    Roles = new List<string>
                    {
                        RoleNames.Admin
                    }
                    //Roles = new List<string>{"User"},
                    //Permissions = new List<string> { "GetStatus"}
                },"password");


                //stack trace
                //pokazuje stacka jak sie odpali w relase
                SetConfig(new HostConfig{ DebugMode = true});

                //redis messaging
                //http://www.codeproject.com/Articles/636730/Distributed-Caching-using-Redis
                container.Register<IRedisClientsManager>(c =>new RedisManagerPool("192.168.254.130:6379"));
                //var mqService = new RedisMqServer(container.Resolve<IRedisClientsManager>());
                var z = new RedisClient("192.168.254.130:6379");


                Plugins.Add(new RazorFormat());
            }
        }


        protected void Application_Start(object sender, EventArgs e)
        {
            new ProteinTrackerAppHost().Init();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (Request.IsLocal)
                Profiler.Start();
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            Profiler.Stop();
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}