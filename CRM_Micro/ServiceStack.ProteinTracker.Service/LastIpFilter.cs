﻿using ServiceStack.Caching;
using ServiceStack.Web;

namespace ServiceStack.ProteinTracker.Service
{
    public class LastIpFilter:ResponseFilterAttribute
    {
        public ICacheClient Cache { get; set; }

        public override void Execute(IRequest req, IResponse res, object responseDto)
        {
            var status = responseDto as StatusResponse;
            if (status!= null)
            {
                status.Message += "Last IP: " + Cache.Get<string>("last IP");
            }
        }
    }
}