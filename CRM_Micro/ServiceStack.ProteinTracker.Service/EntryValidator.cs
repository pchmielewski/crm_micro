﻿using System;
using ServiceStack.FluentValidation;

namespace ServiceStack.ProteinTracker.Service
{
    public class EntryValidator: AbstractValidator<Entry>
    {
        public EntryValidator()
        {
            RuleFor(e => e.Amount).GreaterThan(0);
            RuleFor(e => e.EntryTime).LessThan(DateTime.Now).WithMessage("Date must not be a futhure date");
        
            RuleSet(ApplyTo.Get, () =>
            {
                RuleFor(e => e.Amount.Equals(15));
            });
        }
    }
}