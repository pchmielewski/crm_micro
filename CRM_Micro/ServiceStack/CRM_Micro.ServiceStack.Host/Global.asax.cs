﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Funq;
using ServiceStack;
using ServiceStack.OrmLite;

namespace CRM_Micro.ServiceStack.Host
{
    public class Global : System.Web.HttpApplication
    {

        public class ServiceAppHost:AppHostBase
        {
            public ServiceAppHost()
                : base("CRM_Micro", typeof(ServiceAppHost).Assembly)
            {
            }

            public override void Configure(Container container)
            {
                var factory = new OrmLiteConnectionFactory
            }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            new ServiceAppHost().Init();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}